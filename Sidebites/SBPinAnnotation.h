//
//  SBPinAnnotation.h
//  Sidebites
//
//  Created by Jacob Budin on 9/17/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "SBCafe.h"

@interface SBPinAnnotation : NSObject <MKAnnotation>

@property SBCafe *cafe;
@property (retain, nonatomic) UIView *rightCalloutAccessoryView;

@end