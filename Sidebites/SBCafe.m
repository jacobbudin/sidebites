//
//  SBCafe.m
//  Sidebites
//
//  Created by Jacob Budin on 9/29/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import "SBCafe.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"

@implementation SBCafe

- (id)initWithResultSet:(FMResultSet *)s
{
    self = [super init];
    if (self) {
        self.enclosed = ([s intForColumn:@"type"] == 1) ? YES : NO;
        self.square_feet = [s intForColumn:@"square_feet"];
        self.name = [s stringForColumn:@"name"];
        self.street_address = [s stringForColumn:@"street_address"];
        self.full_address = [s stringForColumn:@"full_address"];
        self.postal_code = [s stringForColumn:@"postal_code"];
        self.phone_number = [s stringForColumn:@"phone_number"];
        self.latitude = [s doubleForColumn:@"latitude"];
        self.longitude = [s doubleForColumn:@"longitude"];
    }
    return self;
}

- (_Bool)isInRegion:(MKCoordinateRegion *)region
{
    return YES;
}

@end
