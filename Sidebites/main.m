//
//  main.m
//  Sidebites
//
//  Created by Jacob Budin on 9/17/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SBAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SBAppDelegate class]));
    }
}
