//
//  SBDetailViewController.m
//  Sidebites
//
//  Created by Jacob Budin on 9/29/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import "SBCafe.h"
#import "SBDetailViewController.h"

@implementation SBDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load dynamic properties
    NSString *phoneNumber = [[self cafe] phone_number];
    NSString *fullAdress = [[self cafe] full_address];
    
    NSArray *fullAddressParts = [fullAdress componentsSeparatedByString:@"\n"];
    NSArray *phoneNumberParts = [NSArray arrayWithObjects:
                            [phoneNumber substringWithRange: NSMakeRange(0,3)],
                            [phoneNumber substringWithRange: NSMakeRange(3,3)],
                            [phoneNumber substringWithRange: NSMakeRange(6,4)],
                            nil];
    
    [[self cafeName] setText:[[self cafe] name]];
    [[self cafeSeating] setText:((self.cafe.enclosed) ? @"Enclosed" : @"Unenclosed")];
    [[self cafePhone] setText:[NSString stringWithFormat:@"+1 (%@) %@-%@", phoneNumberParts[0], phoneNumberParts[1], phoneNumberParts[2]]];
    [[self cafeAddress1] setText:fullAddressParts[0]];
    [[self cafeAddress2] setText:fullAddressParts[1]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showInYelp:(id)sender
{
    if (![[UIApplication sharedApplication]
          canOpenURL:[NSURL URLWithString:@"http:"]] && [[UIApplication sharedApplication]
                                                         canOpenURL:[NSURL URLWithString:@"yelp:"]]) {
        return;
    }
    
    NSArray *fullAddressParts = [[[self cafe] full_address] componentsSeparatedByString:@"\n"];
    NSString *postalCode = [[self cafe] postal_code];
    NSString *terms = [[self cafe] name];
    NSString *location = [NSString stringWithFormat:@"%@, %@", fullAddressParts[0], postalCode];
    
    terms = [terms stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    location = [location stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ([[UIApplication sharedApplication]
        canOpenURL:[NSURL URLWithString:@"yelp:"]]) {
        NSURL *yelp_url = [NSURL
                           URLWithString:[NSString
                                           stringWithFormat:@"yelp:///search?terms=%@&location=%@", terms, location]];
        [[UIApplication sharedApplication] openURL:yelp_url];
    }
    else {
        NSURL *yelp_url = [NSURL
                           URLWithString:[NSString
                                           stringWithFormat:@"http://www.yelp.com/search?find_desc=%@&find_loc=%@", terms, location]];
        [[UIApplication sharedApplication] openURL:yelp_url];
    }
}

- (IBAction)showInMaps:(id)sender
{
    if (![[UIApplication sharedApplication]
        canOpenURL:[NSURL URLWithString:@"http:"]]) {
        return;
    }
    
    NSArray *fullAddressParts = [[[self cafe] full_address] componentsSeparatedByString:@"\n"];
    NSString *location = [NSString
                          stringWithFormat:@"%@, %@, %@", self.cafe.name, fullAddressParts[0], self.cafe.postal_code];
    
    location = [location stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    location = [location stringByReplacingOccurrencesOfString:@"%20" withString:@"+"];
    
    NSInteger zoom = 18;
    NSString *url_str = [NSString stringWithFormat:@"http://maps.apple.com/maps?q=%@&ll=%f,%f&z=%i", location, [[self cafe] latitude], [[self cafe]longitude], zoom];
    url_str = [url_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:url_str];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)showInPhone:(id)sender
{
    if (![[UIApplication sharedApplication]
        canOpenURL:[NSURL URLWithString:@"tel:"]]) {
        return;
    }
    
    NSString *phoneNumber = [[self cafe] phone_number];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]];
    [[UIApplication sharedApplication] openURL:url];
}

@end
