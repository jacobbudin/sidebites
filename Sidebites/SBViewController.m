//
//  SBViewController.m
//  Sidebites
//
//  Created by Jacob Budin on 9/17/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import "SBDetailViewController.h"
#import "SBViewController.h"
#import "SBPinAnnotation.h"
#import "SBCafe.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"

#define NYC_LAT 40.74404817169472
#define NYC_LNG -73.98091263864754
#define DEFAULT_SPAN {0.01, 0.01}

@implementation SBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load cafes from SQLite database
    NSMutableArray *cafes = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[NSBundle mainBundle] pathForResource:@"cafes" ofType:@"db"]];
    if (![db open]) {
        
    }
    
    FMResultSet *s = [db executeQuery:@"SELECT * FROM cafes"];
    while ([s next]) {
        SBCafe* cafe = [[SBCafe alloc] initWithResultSet:s];
        [cafes addObject:cafe];
    }
    
    [self setCafes:cafes];
    
    // Create map, with annotations
    NSMutableArray *mapAnnotations = [[NSMutableArray alloc] initWithCapacity:[[self cafes] count]];
    [self setMapAnnotations:mapAnnotations];
    [self loadMap];
    [self loadAnnotations];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)loadMap
{
    [[self mapView] setShowsUserLocation:YES];
    
    MKCoordinateSpan span = DEFAULT_SPAN;
    CLLocationCoordinate2D center = {NYC_LAT, NYC_LNG};
    MKCoordinateRegion region = {center, span};
    
    [[self mapView] setRegion:region];
    [self centerOnUser:NO];
}

- (IBAction)locateMoi:(id)sender
{
    [self centerOnUser:YES];
}

- (void)centerOnUser:(BOOL)alert
{
    // Start location manager
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    [locationManager setDelegate:(id)self];
    [locationManager startUpdatingLocation];
    [self setLocationManager:locationManager];
    
    // Initiate timer
    if(alert && !self.locationTimer){
        self.locationTimer = [NSTimer
                              scheduledTimerWithTimeInterval:3
                              target:self
                              selector:@selector(checkToSeeIfLocationUpdated:)
                              userInfo:nil
                              repeats:NO];
    }
}

- (void)checkToSeeIfLocationUpdated:(NSTimer *)timer
{
    // Invalidate timer
    [[self locationTimer] invalidate];
    [self setLocationTimer:nil];
    
    // Warn user
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Location Not Found" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // Invalidate timer
    [[self locationTimer] invalidate];
    [self setLocationTimer:nil];
    
    [manager stopUpdatingLocation];
    
    // Check to see whether in New York City area
    if (![self isInSupportedArea:newLocation]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Out of Bounds" message:@"You are not currently located in the New York City region." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    MKCoordinateSpan span = DEFAULT_SPAN;
    CLLocationCoordinate2D center = [newLocation coordinate];
    MKCoordinateRegion region = {center, span};
    
    [self.mapView setRegion:region];
}

- (BOOL)isInSupportedArea:(CLLocation *)newLocation
{
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    NSInteger latitude = coordinate.latitude;
    NSInteger longitude = coordinate.longitude;
    
    if ((latitude < 40)
       || (latitude > 42)
       || (longitude < -75)
       || (longitude > -73)) {
        return NO;
    }
    
    return YES;
}

- (void)loadAnnotations
{
    NSMutableArray *mapAnnotations = [self mapAnnotations];
    MKMapView *mapView = [self mapView];
    
    for (SBCafe *cafe in [self cafes]) {
        SBPinAnnotation *pin = [[SBPinAnnotation alloc] init];
        [pin setCafe:cafe];
        [mapAnnotations addObject:pin];
    }
    
    [mapView addAnnotations:mapAnnotations];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[SBPinAnnotation class]]) {
        static NSString *reuseIdentifier = @"MyMKPinAnnotationView";
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
        
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
            UIButton *calloutView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [annotationView setPinColor:MKPinAnnotationColorRed];
            [annotationView setEnabled:YES];
            [annotationView setCanShowCallout:YES];
            [annotationView setRightCalloutAccessoryView:calloutView];
        } else {
            [annotationView setAnnotation:annotation];
        }
        
        return annotationView;
    }
    
    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    id <MKAnnotation> annotation = [view annotation];
    
    // Load segue to cafe detail
    if ([annotation isKindOfClass:[SBPinAnnotation class]]) {
        SBPinAnnotation *annotation = [view annotation];
        [self setCafe:[annotation cafe]];
        [self performSegueWithIdentifier:@"detail" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Load controller data
    if ([[segue identifier] isEqualToString:@"detail"]) {
        SBDetailViewController* detailViewController = (SBDetailViewController*) [segue destinationViewController];
        [detailViewController setCafe:[self cafe]];
    }
}

@end
