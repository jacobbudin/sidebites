//
//  SBViewController.h
//  Sidebites
//
//  Created by Jacob Budin on 9/17/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SBViewController : UIViewController

@property (nonatomic, strong) SBCafe *cafe;
@property (nonatomic, strong) NSMutableArray *cafes;
@property (nonatomic, strong) NSMutableArray *mapAnnotations;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSTimer *locationTimer;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
