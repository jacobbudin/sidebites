//
//  SBAppDelegate.h
//  Sidebites
//
//  Created by Jacob Budin on 9/17/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface SBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
