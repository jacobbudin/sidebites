//
//  SBDetailViewController.h
//  Sidebites
//
//  Created by Jacob Budin on 9/29/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBCafe.h"

@interface SBDetailViewController : UIViewController

@property SBCafe* cafe;

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBarTitle;
@property (weak, nonatomic) IBOutlet UILabel *cafeSeating;
@property (weak, nonatomic) IBOutlet UILabel *cafeName;
@property (weak, nonatomic) IBOutlet UILabel *cafePhone;
@property (weak, nonatomic) IBOutlet UILabel *cafeAddress1;
@property (weak, nonatomic) IBOutlet UILabel *cafeAddress2;
@property (weak, nonatomic) IBOutlet UIButton *yelpButton;

@end
