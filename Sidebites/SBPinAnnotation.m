//
//  SBPinAnnotation.m
//  Sidebites
//
//  Created by Jacob Budin on 9/17/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import "SBPinAnnotation.h"

@implementation SBPinAnnotation

- (CLLocationCoordinate2D)coordinate
{
    return (CLLocationCoordinate2D) {[[self cafe] latitude], [[self cafe] longitude]};
}

- (NSString*)title
{
    return [[self cafe] name];
}

- (NSString*)subtitle
{
    NSArray *address_parts = [[[self cafe] full_address] componentsSeparatedByString:@"\n"];
    return [address_parts objectAtIndex:0];
}

@end