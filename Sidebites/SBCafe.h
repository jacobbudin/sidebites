//
//  SBCafe.h
//  Sidebites
//
//  Created by Jacob Budin on 9/29/13.
//  Copyright (c) 2013 Jacob Budin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"

@interface SBCafe : NSObject

@property (readonly) NSInteger id;
@property _Bool enclosed;
@property NSInteger square_feet;
@property NSString *name;
@property NSString *street_address;
@property NSString *full_address;
@property NSString *postal_code;
@property NSString *phone_number;
@property CLLocationDegrees latitude;
@property CLLocationDegrees longitude;

- (id)initWithResultSet:(FMResultSet *)s;
- (_Bool)isInRegion:(MKCoordinateRegion*)region;

@end
