require "sqlite3"
require "CSV"
require "Redcarpet"
require "active_support/core_ext/integer/inflections"
require "cgi"
require "htmlentities"

db_file = "Sidebites/cafes.db"
csv_file = "cafes.csv"
csv_headers = ["Type", "License Number", "Cafe Type", "Square Feet", "Entity", "trade_name", ""]
trailing_junk = ["inc", "llc", "corp", "ltd", "incorporated", "co", "company", "corporation", "restaurant", "group", "enterprise", "enterprises", "rest."]

# Open a database
db = SQLite3::Database.new db_file

# Create a database
rows = db.execute <<-SQL
  CREATE TABLE IF NOT EXISTS cafes (
    type INT,
    square_feet INT,
    name TEXT,
    street_address TEXT,
    full_address TEXT,
    postal_code TEXT,
    phone_number TEXT,
    latitude REAL,
    longitude REAL
  );
SQL

# Capitalize (and clean up) words, for restuarants
def capitalize(s)
  if s.class != String
    return s
  end
  words = s.split
  words.map! do |word|
    word.capitalize!
    if word == 'Ny'
      word = 'NY'
    elsif word == 'Ave'
      word = 'Avenue'
    elsif word == 'St'
      word = 'Street'
    end
    word
  end
  return words.join(" ")
end

# More clean-up
lat_long = Regexp.new('\(([0-9\.\-]+),\s*([0-9\.\-]+)\)')
sc_id = Regexp.new('E?\/?S\/c\s+\#[0-9]+', Regexp::IGNORECASE)
ordinalize_me = Regexp.new('([0-9]+)\s+(Avenue|Street)', Regexp::IGNORECASE)

# Execute a few inserts
CSV.foreach(csv_file, { headers: true }) do |row|
  # Type
  type = nil
  case row["Sidewalk Cafe Type"]
  when "Unenclosed"
    type = 0
  when "Enclosed"
    type = 1
  end
  
  # Name
  name = nil
  if row["Camis Trade Name"]
    name = row["Camis Trade Name"]
  else
    name = row["Entity Name"]
  end

  matches = sc_id.match(name)
  if matches and matches.length == 1
    name.sub!(matches[0], "")
  end
  
  name = name.tr(",", " ")
  name = name.split
  name.map! do |word|
    if trailing_junk.include?(word.tr(",.;", "").downcase)
      word = ""
    end
    if word[-1] == ','
      word.slice!(-1)
    end
    word
  end
  name = name.join(" ")
  name = capitalize(name)
  
  # Street address
  street_address = capitalize(row["Street Address"])
  matches = ordinalize_me.match(street_address)
  if matches and matches.length == 3
    replace = matches[1].to_i.ordinalize + " " + matches[2]
    street_address.sub!(matches[0], replace)
  end
  
  # Full address
  full_address = row["Location 1"]
  matches = ordinalize_me.match(full_address)
  if matches and matches.length == 3
    replace = matches[1].to_i.ordinalize + " " + matches[2]
    full_address.sub!(matches[0], replace)
  end
  
  # Coordinates
  latitude = nil
  longitude = nil

  matches = lat_long.match(full_address)
  if matches and matches.length == 3
    full_address.sub!(matches[0], "")
    latitude = matches[1]
    longitude = matches[2]
  end

  full_address_parts = full_address.split("\n")
  full_address_parts.map! do |part|
    capitalize(part)
  end
  full_address = full_address_parts.join("\n")

  name = HTMLEntities.new.decode(Redcarpet::Render::SmartyPants.render(name))
  
  # Skip if establishment is unknown
  if name.length == 0
    next
  end
  
  # Add to database
  db.execute "INSERT INTO cafes VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", type, row["Lic Area Sq Ft"], name, street_address, full_address, row["Address Zip Code"], row["Camis Phone Number"], Float(latitude), Float(longitude)
end
